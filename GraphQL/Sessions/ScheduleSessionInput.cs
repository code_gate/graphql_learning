﻿using System;
using GraphQL.Data;
using HotChocolate.Types.Relay;

namespace GraphQL.Sessions
{
    public class ScheduleSessionInput
    {
        public ScheduleSessionInput(int sessionId, int trackId, DateTimeOffset startTime, DateTimeOffset endTime)
        {
            SessionId = sessionId;
            TrackId = trackId;
            StartTime = startTime;
            EndTime = endTime;
        }

        [ID(nameof(Session))]
        public int SessionId { get; }
        [ID(nameof(Track))]
        public int TrackId { get; }
        public DateTimeOffset StartTime { get; }
        public DateTimeOffset EndTime { get; }
}
        
}