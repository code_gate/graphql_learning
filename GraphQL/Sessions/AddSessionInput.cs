﻿using GraphQL.Data;
using HotChocolate.Types.Relay;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.Sessions
{
    public class AddSessionInput
    {
        public AddSessionInput(string title, string @abstract, IReadOnlyList<int> speakerIds)
        {
            Title = title;
            Abstract = @abstract;
            SpeakerIds = speakerIds;
        }

        public string Title { get; }
        public string? Abstract { get; }

        [ID(nameof(Speaker))]
        public IReadOnlyList<int> SpeakerIds { get; }
    }
}

