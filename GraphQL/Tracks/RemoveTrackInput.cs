﻿using GraphQL.Data;
using HotChocolate.Types.Relay;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.Tracks
{
    public class RemoveTrackInput
    {
        [ID(nameof(Track))]
        public int Id { get; }

        public RemoveTrackInput(int id)
        {
            Id = id;
        }
    }
}
