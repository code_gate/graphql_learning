﻿using GraphQL.Common;
using GraphQL.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.Tracks
{
    public class RemoveTrackPayload : TrackPayloadBase
    {
        public RemoveTrackPayload(Track track) : base(track)
        {
        }

        public RemoveTrackPayload(IReadOnlyList<UserError> errors) : base(errors)
        {
        }
    }
}
