﻿using GraphQL.Data;
using HotChocolate.Types.Relay;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.Tracks
{
    public class RenameTrackInput
    {
        [ID(nameof(Track))]
        public int Id { get; }

        public string Name { get; }

        public RenameTrackInput(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}
