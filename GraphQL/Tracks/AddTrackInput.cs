﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.Tracks
{
    public class AddTrackInput
    {
        public string Name { get; }

        public AddTrackInput(string name)
        {
            Name = name;
        }
    }
}
