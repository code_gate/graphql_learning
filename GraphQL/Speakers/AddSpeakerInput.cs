﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.Speakers
{
    public class AddSpeakerInput
    {
        public AddSpeakerInput(string name, string bio, string webSite)
        {
            Name = name;
            Bio = bio;
            WebSite = webSite;
        }

        public string Name { get; }
        public string? Bio { get; }
        public string? WebSite { get; }
    }
}
